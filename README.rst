========================
SparkInAgri
========================

SparkInAgri is service for plant diseases data extraction.


Requirement
=================

- Python 3.2
- Docker


Setup project
=================

SparkInAgri is open source project publically available on Bitbucket. To clone the repository, use the following command.

.. code-block:: bash

	git clone https://bitbucket.org/sparkindata-irit/sparkinagri.git
	cd sparkinagri
	chmod a+x bin/*



Docker
=================

SparkInAgri is available as a docker image based on the latest Ubuntu version.
The following command allows to build the docker image *sparkinagri*.

.. code-block:: bash

	docker build -t sparkinagri:latest .

In order to run the docker container, be sure that Docker is installed and running then use the next command line.

.. code-block:: bash

	docker run --name=sparkinagri --restart unless-stopped -d -p 7070:7070 sparkinagri



Version
===============

SparkInAgri 0.0.1


Contributors
===============

The following people have contributed to this code:

- Lamjed Ben Jabeur `Lamjed.Ben-Jabeur@irit.fr <mailto:Lamjed.Ben-Jabeur@irit.fr>`_.

License
===============
This software is governed by the `CeCILL-B license <LICENSE.txt>`_ under French law and abiding by the rules of distribution of free software.  You can  use, modify and/ or redistribute the software under the terms of the CeCILL-B license as circulated by CEA, CNRS and INRIA at the following URL
`http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html <http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html>`_.