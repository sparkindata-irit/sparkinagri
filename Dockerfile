FROM ubuntu:latest
MAINTAINER Lamjed Ben Jabeur "Lamjed.Ben-Jabeur@irit.fr"



LABEL vendor=IRIT \
    fr.irit.iris.sparkinclimate.is-beta= \
    fr.irit.iris.sparkinclimate.is-production=  \
    fr.irit.iris.sparkinclimate.version="0.0.1" \
    fr.irit.iris.sparkinclimate.release-date="2016-12-31"

RUN apt-get update && apt-get install -y \
    software-properties-common \
    wget \
    curl \
    libperl4-corelibs-perl

# Install Python 3 and NLTK
RUN apt-get install -y python3 python3-pip
RUN pip3 install --upgrade pip
RUN pip3 install -U nltk
RUN python3 -m nltk.downloader punkt
RUN python3 -m nltk.downloader stopwords
RUN python3 -m nltk.downloader snowball_data

# Install Java 1.8
RUN add-apt-repository ppa:webupd8team/java -y && \
    apt-get update && \
    echo oracle-java7-installer shared/accepted-oracle-license-v1-1 select true | /usr/bin/debconf-set-selections && \
    apt-get install -y oracle-java8-installer && \
    apt-get clean
ENV JAVA_HOME /usr/lib/jvm/java-8-oracle


# Configure project
COPY . /sparkinclimate
WORKDIR /sparkinclimate
RUN pip3 install -r requirements.txt
RUN chmod a+x bin/*


# Install Elasticsearch 5
RUN wget https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-5.1.1.tar.gz
RUN tar -xzf elasticsearch-5.1.1.tar.gz
RUN useradd -m -d /home/elasticsearch elasticsearch
RUN chown -R elasticsearch:elasticsearch elasticsearch-5.1.1/
USER elasticsearch
WORKDIR elasticsearch-5.1.1/
RUN ./bin/elasticsearch -d
USER root
WORKDIR ..


# ENTRYPOINT ["python3"]
# CMD ["bin/server"]
